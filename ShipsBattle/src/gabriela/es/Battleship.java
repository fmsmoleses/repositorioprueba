package gabriela.es;

import java.util.Random;
import java.util.Scanner;

public class Battleship {
	
	static int ShotsFired=0;
	static int ShipsSunk=0;
	static int gamePlayed=0;
	public static String[] barcoHorizontal1x2=new String[2];
	public static String[] barcoVertical1x2=new String[2];
	public static String[] barcoHorizontal1x3=new String[3];
	public static String[] barcoVertical1x3=new String[3];
	public static char[][] tablaBarcos= new char[5][5];
	
	public static void GenerarBarcoVertical1x2() {
		int x,y,origx,origy;
		boolean barcoFormado=true;
		System.out.println("Entro en GenerarBarcoVertical1x2 ");
		do {
			do {
				x = (int)(Math.random() * 10 / 2);
				y = (int)(Math.random() * 10 / 2);
			}while (tablaBarcos[x][y]!='o');
			tablaBarcos[x][y]='x';
			origx=x;
			origy=y;
			barcoVertical1x2[0]=Integer.toString(x)+Integer.toString(y);
			int derecha  = (int)(Math.random() * 10 / 5);
			if(derecha==1) { 
				if ((x+1)<5) {x++;} else {x--;}
			}	
			else
				if ((x-1)<0) x++; else x--;
			
			if (tablaBarcos[x][y]!='o') {
				barcoFormado=false;
				tablaBarcos[origx][origy]='o';
				}
			else {
				barcoVertical1x2[1]=Integer.toString(x)+Integer.toString(y);
				barcoFormado=true;
				}
		}while(!barcoFormado);
		tablaBarcos[x][y]='x';
		System.out.println(barcoVertical1x2[0]+" "+barcoVertical1x2[1]);
	}
	
	public static void GenerarBarcoVertical1x3() {
		int x,y,origx,origy,orig2x,orig2y;
		boolean barcoFormado=true;
		System.out.println("Entro en GenerarBarcoVertical1x3 ");
		do {
			do {
				x = (int)(Math.random() * 10 / 2);
				y = (int)(Math.random() * 10 / 2);
			}while (tablaBarcos[x][y]!='o');
			tablaBarcos[x][y]='x';
			origx=x;
			origy=y;
			barcoVertical1x3[0]=Integer.toString(x)+Integer.toString(y);
			int derecha  = (int)(Math.random() * 10 / 5);
			if(derecha==1) { 
				if ((x+1)<5) {x++;} else {x--;}
			}	
			else
				if ((x-1)<0) x++; else x--;
			
			if (tablaBarcos[x][y]!='o') {
				barcoFormado=false;
				tablaBarcos[origx][origy]='o';
				}
			else {
				barcoVertical1x3[1]=Integer.toString(x)+Integer.toString(y);
				tablaBarcos[x][y]='x';
				orig2x=x;
				orig2y=y;
				if(derecha==1) { 
					if ((x+1)<5) {x++;} else {x--;}
				}	
				else
					if ((x-1)<0) x++; else x--;
				if (tablaBarcos[x][y]!='o') {
					barcoFormado=false;
					tablaBarcos[origx][origy]='o';
					tablaBarcos[orig2x][orig2y]='o';
					}
				else {
					barcoVertical1x3[2]=Integer.toString(x)+Integer.toString(y);
					barcoFormado=true;
					}
				}
		}while(!barcoFormado);
		tablaBarcos[x][y]='x';
		System.out.println(barcoVertical1x3[0]+" "+barcoVertical1x3[1]+" "+barcoVertical1x3[2]);
	}
	

	public static void GenerarBarcoHorizontal1x3() {
		int x,y,origx,origy,orig2x,orig2y;
		boolean barcoFormado=true;
		System.out.println("Entro en GenerarBarcoHorizontal1x3 ");
		do {
			do {
				x = (int)(Math.random() * 10 / 2);
				y = (int)(Math.random() * 10 / 2);
			}while (tablaBarcos[x][y]!='o');
			tablaBarcos[x][y]='x';
			origx=x;
			origy=y;
			barcoHorizontal1x3[0]=Integer.toString(x)+Integer.toString(y);
			int abajo  = (int)(Math.random() * 10 / 5);
			if(abajo==1) { 
				if ((y+1)<5) {y++;} else {y--;}
			}	
			else
				if ((y-1)<0) y++; else y--;
			
			if (tablaBarcos[x][y]!='o') {
				barcoFormado=false;
				tablaBarcos[origx][origy]='o';
				}
 			else {
				barcoHorizontal1x3[1]=Integer.toString(x)+Integer.toString(y);
				tablaBarcos[x][y]='x';
				orig2x=x;
				orig2y=y;
				if(abajo==1) { 
					if ((y+1)<5) {y++;} else {y--;}
				}	
				else
					if ((y-1)<0) y++; else y--;
				if (tablaBarcos[x][y]!='o') {
					barcoFormado=false;
					tablaBarcos[origx][origy]='o';
					tablaBarcos[orig2x][orig2y]='o';
					}
				else {
					barcoHorizontal1x3[2]=Integer.toString(x)+Integer.toString(y);
					barcoFormado=true;
					}
				}

		}while(!barcoFormado);
		tablaBarcos[x][y]='x';
		System.out.println(barcoHorizontal1x3[0]+" "+barcoHorizontal1x3[1]+" "+barcoHorizontal1x3[2]);
	}

	
	
	public static void GenerarBarcoHorizontal1x2() {
		int x,y,origx,origy;
		boolean barcoFormado=true;
		System.out.println("Entro en GenerarBarcoHorizontal1x2 ");
		do {
			do {
				x = (int)(Math.random() * 10 / 2);
				y = (int)(Math.random() * 10 / 2);
			}while (tablaBarcos[x][y]!='o');
			tablaBarcos[x][y]='x';
			origx=x;
			origy=y;
			barcoHorizontal1x2[0]=Integer.toString(x)+Integer.toString(y);
			int abajo  = (int)(Math.random() * 10 / 5);
			if(abajo==1) { 
				if ((y+1)<5) {y++;} else {y--;}
			}	
			else
				if ((y-1)<0) y++; else y--;
			
			if (tablaBarcos[x][y]!='o') {
				barcoFormado=false;
				tablaBarcos[origx][origy]='o';
				}
			else {
				barcoHorizontal1x2[1]=Integer.toString(x)+Integer.toString(y);
				barcoFormado=true;
				}
		}while(!barcoFormado);
		tablaBarcos[x][y]='x';
		System.out.println(barcoHorizontal1x2[0]+" "+barcoHorizontal1x2[1]);
	}
	
	public static void generarTablero() {
		boolean completa=true;
		Random aleatorio = new Random(System.currentTimeMillis());
		//Generar tablas de numeros aleatorios
		//inicializar tablaBarcos
		for (int i=0;i<tablaBarcos.length;i++) {
			for(int j=0;j<tablaBarcos[i].length;j++) {
				tablaBarcos[i][j]='o';
			}
		}
		
		int[] tablaAleatorios=new int[4];

		for (int i = 0; i<tablaAleatorios.length;i++) {
			tablaAleatorios[i] = i;
		}

		Random r = new Random();
		for (int i = tablaAleatorios.length; i > 0; i--) {
		    int posicion = r.nextInt(i);
		    int tmp = tablaAleatorios[i-1];
		    tablaAleatorios[i - 1] = tablaAleatorios[posicion];
		    tablaAleatorios[posicion] = tmp;
		}

		System.out.print("Tabla Aleatorios:");
		for (int i = 0; i<tablaAleatorios.length;i++) {
			System.out.print(tablaAleatorios[i]+" ");
		}
		
		System.out.println("\nBarcos Generados:");
		for (int i=0;i<tablaAleatorios.length;i++) {
			switch (tablaAleatorios[i]) {
			case 0: 
				GenerarBarcoHorizontal1x2();                                       
				break;
			case 1:
				GenerarBarcoVertical1x2();                                       
				break;
			case 2:
				GenerarBarcoHorizontal1x3();                                       
				break;
			case 3:
				GenerarBarcoVertical1x3();                                       
				break;
			}
		}
		
 
		
	}	
	
	public static void pintaPantallaBasic()	{
		
		System.out.println("-------------------");
		System.out.println("BARCOS INTRODUCIDOS");
		System.out.println("-------------------");
		System.out.println("  A    B   C   D   E  ");
		System.out.println(" ┌───┬───┬───┬───┬───┐");
		for (int i=0;i<tablaBarcos.length;i++) {
			System.out.print(i+"│");
			for(int j=0;j<tablaBarcos[i].length;j++) {
				System.out.print(" "+tablaBarcos[i][j]+" │");
			}
			if (i<tablaBarcos.length-1)
				System.out.println("\n ├───┼───┼───┼───┼───┤");
		}
		System.out.println("\n └───┴───┴───┴───┴───┘");
		
		System.out.println("----------------");
		System.out.println("PANTALLA JUGADOR");
		System.out.println("----------------");
		System.out.println("  A    B   C   D   E  ");
		System.out.println(" ┌───┬───┬───┬───┬───┐");
		for (int i=0;i<tablaBarcos.length;i++) {
			System.out.print(i+"│");
			for(int j=0;j<tablaBarcos[i].length;j++) {
				if (tablaBarcos[i][j]=='A'||tablaBarcos[i][j]=='#') {
				System.out.print(" "+tablaBarcos[i][j]+" │");
				}else {System.out.print("   │");}
			}
			if (i<tablaBarcos.length-1)
				System.out.println("\n ├───┼───┼───┼───┼───┤");
		}
		System.out.println("\n └───┴───┴───┴───┴───┘");
		
		System.out.println("Shots fired:" + ShotsFired);
		System.out.println("Ships sunk:" + ShipsSunk);
	
		
	}
	
	
	public static void pintarBasic() {
		
		int x=0,y=0;
		String posY="", coordenates="";
		boolean todosHundidos = false, bien= false;
		Scanner input =new Scanner(System.in);
		do {
			pintaPantallaBasic();
			
			//Se introducen coordenadas
			do {
				bien=true;
				System.out.println("Insert the cordenates:\n>");
				coordenates=input.nextLine();
				if (!coordenates.endsWith(")")||!coordenates.startsWith("(")||!coordenates.contains(",")||coordenates.length()!=5) {
					bien=false;
				}
				if(bien) {
					String coord1=coordenates.substring(1, 2);
					posY=coordenates.substring(3, 4);
					if (!coord1.equals("0")&&!coord1.equals("1")&&!coord1.equals("2")&&!coord1.equals("3")&&!coord1.equals("4")) {bien =false;}
					if (!posY.toUpperCase().equals("A")&&!posY.toUpperCase().equals("B")&&!posY.toUpperCase().equals("C")&&!posY.toUpperCase().equals("D")&&!posY.toUpperCase().equals("E")){
						bien=false;
					}
				}
				if (bien) {
					//Calculamos las coordenadas numericas
					x=Integer.parseInt(coordenates.substring(1, 2));
					switch (posY.toUpperCase()) {
					case "A": y=0; break;
					case "B": y=1; break;
					case "C": y=2; break;
					case "D": y=3; break;
					case "E": y=4; break;
					}
				}
			    if (!bien) {
			    	System.out.println("The coordinates are not valid.");
			    }
				
			}while (!bien);

			if (tablaBarcos[x][y]=='o'||tablaBarcos[x][y]=='A') {
				tablaBarcos[x][y]='A';
			}
			else {
				tablaBarcos[x][y]='#';
				//verificar Hundido 
				verificarHundido(x,y);
				
			}
			ShotsFired++;
			pintaPantallaBasic();
			
		}while(ShipsSunk<4);
		
		
	}	
	
	public static void verificarHundido(int x, int y) {
		
		System.out.println("Entro en verificarHundido x="+x+" y="+y);
		//verificar Hundido barcoHorizontal1x2.
		boolean estaContenido=false;
		boolean hundido=true;
		String cad=Integer.toString(x)+Integer.toString(y);
		for (int i=0;i<barcoHorizontal1x2.length&&hundido;i++) {
			if (barcoHorizontal1x2[i].contains(cad)) {
				estaContenido=true;
				for(int j= 0;j<barcoHorizontal1x2.length&&hundido;j++) {
					int tabx=Integer.parseInt(barcoHorizontal1x2[j].substring(0,1));
					int taby=Integer.parseInt(barcoHorizontal1x2[j].substring(1,2));
					
					if (tablaBarcos[tabx][taby]!='#') hundido=false;
				}
			}
		}
		if (hundido&&estaContenido) ShipsSunk++;
		
		//verificar Hundido barcoVertical1x2.
		estaContenido=false;
		hundido=true;
		for (int i=0;i<barcoVertical1x2.length&&hundido;i++) {
			if (barcoVertical1x2[i].contains(cad)) {
				estaContenido=true;
				for(int j= 0;j<barcoVertical1x2.length&&hundido;j++) {
					int tabx=Integer.parseInt(barcoVertical1x2[j].substring(0,1));
					int taby=Integer.parseInt(barcoVertical1x2[j].substring(1,2));
					
					if (tablaBarcos[tabx][taby]!='#') hundido=false;
				}
			}
		}
		if (hundido&&estaContenido) ShipsSunk++;
		
		//verificar Hundido barcoHorizontal1x3.
		estaContenido=false;
		hundido=true;
		for (int i=0;i<barcoHorizontal1x3.length&&hundido;i++) {
			if (barcoHorizontal1x3[i].contains(cad)) {
				estaContenido=true;
				for(int j= 0;j<barcoHorizontal1x3.length&&hundido;j++) {
					int tabx=Integer.parseInt(barcoHorizontal1x3[j].substring(0,1));
					int taby=Integer.parseInt(barcoHorizontal1x3[j].substring(1,2));
					
					if (tablaBarcos[tabx][taby]!='#') hundido=false;
				}
			}
		}
		if (hundido&&estaContenido) ShipsSunk++;

		//verificar Hundido barcoVertical1x3.
		estaContenido=false;
		hundido=true;
		for (int i=0;i<barcoVertical1x3.length&&hundido;i++) {
			if (barcoVertical1x3[i].contains(cad)) {
				estaContenido=true;
				for(int j= 0;j<barcoVertical1x3.length&&hundido;j++) {
					int tabx=Integer.parseInt(barcoVertical1x3[j].substring(0,1));
					int taby=Integer.parseInt(barcoVertical1x3[j].substring(1,2));
					
					if (tablaBarcos[tabx][taby]!='#') hundido=false;
				}
			}
		}
		if (hundido&&estaContenido) ShipsSunk++;
		
		
		
	}
	
	
	
	
	public static void pintarComplex(int rows, int columns) {
		
		System.out.println("  A    B   C   D   E  ");
		System.out.println(" ┌───┬───┬───┬───┬───┐");
		System.out.println("0│   │   │   │   │   │");
		System.out.println(" ├───┼───┼───┼───┼───┤");
		System.out.println("1│   │   │   │   │   │");
		System.out.println(" ├───┼───┼───┼───┼───┤");
		System.out.println("2│   │   │   │   │   │");
		System.out.println(" ├───┼───┼───┼───┼───┤");
		System.out.println("3│   │   │   │   │   │");
		System.out.println(" ├───┼───┼───┼───┼───┤");
		System.out.println("4│   │   │   │   │   │");
		System.out.println(" └───┴───┴───┴───┴───┘");
		
		System.out.println("Shots fired:" + ShotsFired);
		System.out.println("Ships sunk:" + ShipsSunk);
		
	}	
	
	public static void main(String[] args) {
		Scanner input = new Scanner (System.in);
		String modo;
		int rows, columns;
		String quieroVolverAJugar="Y";
		
		do {
			System.out.println("Battleship");
			do {
				System.out.print("Which mode do you want to play?\n>"); 
				modo=input.nextLine();
				if (modo.equals("Basic")){
					
					generarTablero();
					pintarBasic();}
				else if(modo.equals("Complex")){
					do {
						System.out.print("Enter number of rows(6-10)?\n>");
						rows=input.nextInt();
					}while (rows<6 && rows>10);
					do {
						System.out.print("Enter number of columns(6-10)?\n>");
						columns=input.nextInt();
					}while (columns<6 && columns>10);
					pintarComplex(rows, columns);}
			}while(!modo.equals("Basic")&&!modo.equals("Complex"));
			
			if (ShipsSunk==4) {
				System.out.println("The game is over!");
				gamePlayed++;
				System.out.println(gamePlayed+" Game Played");
				System.out.println("Do you want to play again? (Y/N):");
				quieroVolverAJugar=input.nextLine();
				}
			ShipsSunk=0;
			ShotsFired=0;
			
		}while(quieroVolverAJugar.toUpperCase().equals("Y"));

		System.out.println("Thanks for playing!!");
	}

}
