package es.GabrielaMoles;

import java.util.Scanner;

public class SpecialCalculator {

	public static void main(String[] args) {
		int num1 = 0;
		int num2 = 0;
		String option;
		final String LCM = "A";
		final String GCD = "B";
		final String RDM = "C";
		final String DIF = "D";
		Scanner input = new Scanner (System.in);
		do {
			System.out.println("Enter first positive number:");
			num1=input.nextInt();
			if (num1<0){
				System.out.println("Sorry error you must enter posive number, try again.");
			}
		}while (num1<0);
		do {
			System.out.println("Enter second positive number:");
			num2=input.nextInt();
			if (num2<0){
				System.out.println("Sorry error you must enter posive number, try again.");
			}
		}while (num2<0);
		do {
			System.out.println("(A) Least common multiple.");
			System.out.println("(B) Greatest common divisor,");
			System.out.println("(C) Random integer value in the range defined by the introduced numbers.");
			System.out.println("(D) Difference between the squared roots of the numbers.\n\n");
			System.out.println("Select an option please(A,B,C,D)\n");
			//option= (int)input.nextByte();
			
			switch (option=input.next()) {
			case LCM:
		        int lcm=1;
		        int i=1; //we must start with least divisor = 2
		        while(i <= num1 || i <= num2)
		        {
		            if(num1%i==0 || num2%i==0) //if division is exact (remainder==0)
		            {
		            lcm=lcm*i;
		            if(num1%i==0) num1=num1/i;
		            if(num2%i==0) num2=num2/i;
		            }
		            else
		                i=i+1;               
		        }                        
		        System.out.println("Least common multiple:" +lcm);                                          
		        
				break;
			case GCD:
				int aux;
				int gcd=num1;
				while (num2 != 0) {
				     aux = num2;
				     num2 = num1 % num2;
				     gcd = aux;
				}
				System.out.println("Greatest common divisor:" +gcd);
				
				break;
			case RDM:
				int numberAleat;
				if (num1>num2) {
					numberAleat= (int)(Math.random()*(num1-num2+1)+num2);
					}
				else {
					numberAleat= (int)(Math.random()*(num2-num1+1)+num1);
					}
				System.out.println("Random integer value in the range defined by the introduced numbers:"+numberAleat);
				break;
			case DIF:
				System.out.println("Difference between the squared roots of the numbers:"+(Math.sqrt(num1)-Math.sqrt(num2)));
				break;
			default:
				System.out.println("Error in selection, try again.");
				
			}
			
			
		}while (!option.equals("A") && !option.equals("B") && !option.equals("C") && !option.equals("D"));
		
		
	}

}
