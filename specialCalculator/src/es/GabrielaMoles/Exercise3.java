package es.GabrielaMoles;

import java.util.Scanner;

public class Exercise3 {
    public static void main (String[] args){
        Scanner input = new Scanner(System.in);
        int nVisitors, price = 0;
       
        System.out.println(
                "MUSEUM TICKETS PRICE\n" +
                "The total price of the tickets to visit the museum is:\n" +
                "1) Base fixed price = � 10, plus;\n" +
                "2) 5� per visitor when the number of visitors is less than 8, or;\n" +
                "3) 4� per visitor when the number of visitors is between 8 and 15, or;\n" +
                "4) 3� per visitor when the number of visitors is greater than 15.\n");
        nVisitors = input.nextInt();
 System.out.println("How many people are visiting the museum? ");
        if(nVisitors<=0)
            System.out.println("ERROR: The number of visitors must be greater than 0 (zero)\n");
        else{   
            if(nVisitors<8)
                price = 10 + nVisitors*7;
            else if (nVisitors >= 8 && nVisitors < 15)
                price = 10 + nVisitors*4;
            else
                price = 10 + nVisitors*3;
            System.out.println("\nThe total price of the tickets is " + price + ".");
        }
    }

}
