package fernando.es;

public class ClaseBicicleta {

	public static void main(String[] args) {
		System.out.println("Programa de pruebas de clases");
		System.out.println("-----------------------------");
		Bicicleta miBicicleta= new Bicicleta(30, 45, 7);
		Bicicleta tuBicicleta= new Bicicleta(40, 35, 11);
		System.out.println(miBicicleta.getVelocidad());
		System.out.println(tuBicicleta.getVelocidad());
		
		System.out.println("---------");
		
		miBicicleta.brake();
		System.out.println(miBicicleta.getVelocidad());
		tuBicicleta.speedUp();
		System.out.println(tuBicicleta.getVelocidad());
		System.out.println("---------");
		miBicicleta.setVelocidad(456);
		
		System.out.println("miBicicleta");
		System.out.println("-----------");
		System.out.println(miBicicleta.getVelocidad());
		System.out.println(miBicicleta.getCadencia());
		System.out.println(miBicicleta.getEngranaje());
		System.out.println("tuBicicleta");
		System.out.println("-----------");
		System.out.println(tuBicicleta.getVelocidad());
		System.out.println(tuBicicleta.getCadencia());
		System.out.println(tuBicicleta.getEngranaje());
		
		System.out.println("Fin pruebas de clases");
		System.out.println("---------------------");
		
	}

}
