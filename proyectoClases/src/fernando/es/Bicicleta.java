package fernando.es;

public class Bicicleta {
	
	private int velocidad;
	private int cadencia;
	private int engranaje;
	
	public Bicicleta(int velocidad, int cadencia, int engranaje) {
		this.velocidad = velocidad;
		this.cadencia = cadencia;
		this.engranaje = engranaje;
	}

	public void speedUp() {
		this.velocidad=this.velocidad*2;
	}
	
	public void brake() {
		this.velocidad=this.velocidad/2;
	}



	public void setCadencia(int cadencia) {
		this.cadencia = cadencia;
	}


	public void setEngranaje(int engranaje) {
		this.engranaje = engranaje;
	}

	public int getVelocidad() {
		return velocidad;
	}

	public void setVelocidad(int velocidad) {
		this.velocidad = velocidad;
	}

	public int getCadencia() {
		return cadencia;
	}

	public int getEngranaje() {
		return engranaje;
	}
	
	
	
}
